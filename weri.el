;;; weri.el --- Was ERC Robot Initially.

;; Author: David Edmondson (dme@dme.org)
;;         Daniel Cerqueira (dan.git@lispclub.com)
;; Maintainer: Daniel Cerqueira (dan.git@lispclub.com)
;; Version: 0.7
;; Keywords: ERC, IRC, chat, robot, bot
;; Homepage: https://gitlab.com/alexandre1985/weri/
;; SPDX-License-Identifier: GPL-3.0-or-later

;; Copyright (C) 2002 David Edmondson, 2024 Daniel Cerqueira

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see
;; <https://www.gnu.org/licenses/>

;;; Commentary:

;; This code improves the erc-robot.el .

;; Installation:

;; The robot uses hooks to gain access to ERC.  The following need to
;; be executed after ERC has loaded:

;; (load-library "weri")
;; (add-hook 'erc-server-PRIVMSG-functions 'weri-remote 90)
;; (add-hook 'erc-send-completed-hook 'weri-local 90)

;; It is particularly important that the remote robot function is added
;; to the tail of the PRIVMSG hook.

;; Robot commands are declared using the list "weri-commands".
;; An example might be:

;; (setq weri-commands
;;       '(
;;         ("hello" t (lambda (args)
;;                      "Greet someone."
;;                      "Hello to you too! :-)"))
;;         ("echo" nil (lambda (args)
;;                       "Echo a message or run an IRC command. For example: !echo hello to all , or !echo /kick fred"
;;                       args))
;;         ("el" t (lambda (args)
;;                   "Evaluate an Emacs Lisp expression. !el <lisp-expression>"
;;                   (let ((prohibited-word
;;                          (find-and 'string-search
;;                                    '("shell" "command" "process" "run" "machine"
;;                                      "system" "passwd" "groups" "shadow" "delete-"
;;                                      "concat" "format" "erc-" "suspend-pc")
;;                                    args)))
;;                     (if prohibited-word
;;                         (format
;;                          "ERROR => Prohibited word \"%s\" found. Cannot eval such expression."
;;                          prohibited-word)
;;                       (let* ((sexp-string-formated (string-replace "\"" "\\\"" args))
;;                              (message (string-trim
;;                                        (truncate-string-to-width
;;                                         (shell-command-to-string
;;                                          (concat
;;                                           "LC_ALL=C firejail --quiet "
;;                                           "timeout 3 "
;;                                           "emacs -q --batch --eval="
;;                                           "\"(prin1 " sexp-string-formated ")\""))
;;                                         1501 0 nil t))))
;;                         (if (string-match "^\\(\\w.+\\)$" message)
;;                             (replace-regexp-in-string "^" "ERROR => "
;;                                                       (match-string 1 message))
;;                           (replace-regexp-in-string "^" "RESULT => " message)))))))
;;         ("song" t (lambda (args)
;;                     "Print the song name that I am currently listening to."
;;                     (emms-show)))
;;         ("version" t erc-version) ;; erc-version is a function without arguments
;;         ))

;; The robot already has 2 built-in commands. The "!cmds" to list all
;; functions, and the "!help" to show the docstring of a command.

;; The variable "weri-channels" can be set. It configures from which
;; channels does the robot receives commands. Default is all channels
;; and all query messages.
;; An example might be:

;; (setq weri-channels "^\\(#\\(?:lisp\\|bots\\)\\|[^#\n]+\\)$")

;; This configures the robot to receive commands at the #lisp and
;; #bots channels, and also in any nick query chat.

;; Code:

(require 'erc)

;; compatibility
(when (featurep 'xemacs)
  (defun replace-regexp-in-string
      (regexp rep string &optional fixedcase literal subexp start)
    (replace-in-string string regexp rep literal)))

;; enable/disable main functions
(defun weri-enable ()
  "Enable WERI."
  (interactive)
  (cond
   ((null (and (boundp 'weri-commands) (consp weri-commands)))
    (error "weri-commands variable needs to be configured as a list."))
   ((and (memq 'weri-remote erc-server-PRIVMSG-functions)
         (memq 'weri-local erc-send-completed-hook))
    (message "WERI already was enabled."))
   (t (progn
        (add-hook 'erc-server-PRIVMSG-functions 'weri-remote 90)
        (add-hook 'erc-send-completed-hook 'weri-local 90)
        (message "WERI enabled.")))))

(defun weri-disable ()
  "Disable WERI."
  (interactive)
  (if (not (or
            (memq 'weri-remote erc-server-PRIVMSG-functions)
            (memq 'weri-local erc-send-completed-hook)))
      (message "WERI already was disabled.")
    (remove-hook 'erc-server-PRIVMSG-functions 'weri-remote)
    (remove-hook 'erc-send-completed-hook 'weri-local)
    (message "WERI disabled.")))

;; face
(defface weri-face '((t (:foreground "magenta")))
  "Face used for your robot's output."
  :group 'erc-faces)

(defface weri-cmdname-face '((t (:foreground "green")))
  "Face used for robot's command name."
  :group 'erc-faces)

;; variables
(defcustom weri-commands nil
  "A list of robot commands and the functions which implement them."
  :group 'erc
  :type '(repeat (list
                  string
                  (choice
                   (const :tag "Permission to all nicks" t)
                   (regexp :tag "Regexp of permitted nicks")
                   (const :tag "Permission to current nick only" nil))
                  function)))

(defcustom weri-channels ".+"
  "A regexp of channels where the robot is actively set."
  :group 'erc
  :type 'regexp)

(defcustom weri-prohibited-nicks regexp-unmatchable
  "A regexp of nicknames that are prohibited from interacting with the robot."
  :group 'erc
  :type 'regexp)

;; auxiliary functions
(defun weri-channel-name (string)
  "Get only the name of the channel from STRING. Return NIL if STRING not a string."
  (when (stringp string)
    (substring string 0 (seq-position string ?@))))

(defun weri-function-docstring (atom)
  "Get the Docstring of an GNU Emacs ATOM function. Return T if ATOM function is in C or a autoloaded functions. NIL otherwise."
  (let* ((func-lambda (symbol-function atom))
         (is-list (listp func-lambda))
         (func-type (cond
                     (is-list (car func-lambda))
                     ((functionp func-lambda) 'function)
                     (t 'other)))
         (docstring (cond
                     ((eq func-type 'lambda)
                      (caddr func-lambda))
                     ((eq func-type 'closure)
                      (if (<= (length func-lambda) 4)
                          nil
                        (cadddr func-lambda)))
                     ((eq func-type 'function) (documentation atom))
                     (t nil))))
    (cond
     ((eq func-type 'other) t)
     ((stringp docstring) (substring docstring 0 (seq-position docstring ?\n)))
     (t nil))))

(defun weri-cmd-output-parser (output)
  "Parses a robot command output. To deal with non-string command output; also to deal with empty string command output."
  (cond
   ((and (stringp output) (zerop (length output)))
    "(Command without output)")
   ((> (length (string-split output "\n")) 5)
    "ERROR => Output has too many lines. Maximum is 5 lines.")
   ((stringp output) output)
   (t "(Command without a string output)")))


;; built-in command functions
(defun weri-help-command (args)
  "WERI helper. !help <cmd-name>"
  (let* ((cmd-arg (substring args 0 (seq-position args ? )))
         (target (assoc cmd-arg weri-commands))
         (name (car target))
         (function (caddr target))
         (docstring (cond
                     ((atom function) (weri-function-docstring function))
                     ((<= (length function) 3) nil)
                     (t (caddr function)))))
    (cond
     ((zerop (length args))
      "USAGE: !help <cmd-name>. Use !cmds to list all commands.")
     ((string= cmd-arg "cmds")
      "!cmds => Show all commands.")
     ((string= cmd-arg "help")
      "!help => WERI helper. !help <cmd-name>")
     ((null target)
      (format
       "ERROR => !%s not available. Use !cmds to list all commands."
       cmd-arg))
     ((null (functionp function))
      (format "SYSTEM ERROR: Command !%s has the inexistent function %s."
              name function))
     ((null docstring)
      (format "ERROR => !%s does not have a Docstring." cmd-arg))
     ((stringp docstring)
      (format "!%s => %s" name docstring))
     (t "ERROR => Cannot get the function Docstring."))))

(defun weri-cmds-command (builtin-cmds)
  "Show all commands. !cmds"
  (concat "All commands: "
          (string-join builtin-cmds " ")
          " "
          (mapconcat 'car weri-commands " ")))

;; functions
(defun weri-remote (proc parsed)
  "Implements a simple robot for ERC.  Messages to the robot are of the form:
\"nick: !command args\", where:
nick    - the nickname of the user who is the target of the command, i.e. the
          person running the robot,
command - the specific command,
args    - arguments to the command (optional)."
  (let* ((sspec (aref parsed 1))
         (nick (substring (nth 0 (erc-parse-user sspec)) 1))
         (tgt (car (aref parsed 4)))
         (msg (aref parsed 5)))
    (when (not (string= nick (erc-current-nick)))
      (weri-command proc nick
                    (if (string-match "^#" tgt) tgt nick)
                    msg nil)))
  nil)

(defun weri-local (str)
  "Funnel text typed by the local user to the local robot.  See
\"weri-remote\" for details of the command format."
  (weri-command erc-server-process (erc-current-nick) (buffer-name) str t))

(defun weri-command (proc from tgt msg locally-generated)
  "Robot worker."
  (let ((me (erc-current-nick))
        (channel-buffer (erc-get-buffer tgt proc))
        (builtin-cmds '("cmds" "help")))
    (when (and
           (null (string-match weri-prohibited-nicks (regexp-quote from)))
           channel-buffer
           (when (boundp 'weri-channels)
             (string-match weri-channels
                           (weri-channel-name (buffer-name channel-buffer))))
           weri-commands
           (string-match (concat "^" (regexp-quote me)
                                 ": !\\([^ ]+\\) ?\\(.*\\)")
                         msg))      ; this is a robot command.
      (let* ((cmd (substring msg (match-beginning 1) (match-end 1)))
             (args (string-trim (substring msg (match-beginning 2))))
             (target-list (cond
                           ((member cmd builtin-cmds) (list cmd t t))
                           (t (assoc cmd weri-commands))))
             (allowed-users (nth 1 target-list))
             (function (nth 2 target-list))
             (permitted (or locally-generated
                            (eq t allowed-users)
                            (and (stringp allowed-users)
                                 (string-match allowed-users
                                               (regexp-quote from)))))
             (weri-prefix (propertize
                           (if (or (null target-list)
                                   (null permitted))
                               "[WERI]"
                             (format "[WERI:%s]" (upcase cmd)))
                           'face 'weri-cmdname-face))
             (reply
              (propertize
               (cond
                ((string= cmd "cmds") (weri-cmds-command builtin-cmds))
                ((string= cmd "help") (weri-help-command args))
                ((null target-list) (format "ERROR: !%s is unknown. Try !cmds" cmd))
                ((null (functionp function))
                 (format "SYSTEM ERROR: Command !%s has the inexistent function %s."
                         cmd function))
                ((null permitted) (format "ERROR: No access to !%s for %s." cmd from))
                ((atom function) (weri-cmd-output-parser (funcall function)))
                (t (weri-cmd-output-parser (funcall function args))))
               'face 'weri-face))
             ;; dme: this version allows you to cause the robot to do things, for
             ;;       example you can try "dme: !echo /kick fred", and it will send
             ;;       to the server "/kick fred", rather than "joe: /kick fred".
             (full-reply (cond
                          ((string-match "^/" reply) reply)
                          (t (replace-regexp-in-string
                              "^"
                              (format "%s: %s " from weri-prefix)
                              reply)))))

        (erc-log (substring-no-properties full-reply))
        (save-excursion
          (set-buffer channel-buffer)
          (let* ((inhibit-read-only t)
                 (lines (string-lines full-reply t))
                 (multiline-p (> (length lines) 1))
                 p)
            (mapc
             (lambda (line)
               (goto-char (point-max))
               (setq p (re-search-backward (erc-prompt)))
               (insert "<" (propertize me 'face 'erc-nick-default-face) "> "
                       line "\n")
               (save-excursion
                 (save-match-data
                   (save-restriction
                     (narrow-to-region p (point))
                     (run-hook-with-args 'erc-send-modify-hook)
                     (run-hook-with-args 'erc-send-post-hook))))
               (set-marker (process-mark erc-server-process) (point))
               (set-marker erc-insert-marker (point))
               (goto-char (point-max))

               (erc-process-input-line (concat line "\n") nil multiline-p))
             lines)))))))

(provide 'weri)
